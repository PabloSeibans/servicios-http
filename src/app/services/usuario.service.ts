import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter, take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url: string = 'https://randomuser.me/api/';

  constructor(public http: HttpClient) { }

  obtenerUsuario(): Observable<any>{
    return this.http.get<any>(this.url);
  }

  obtenerUsuarioMasculino(): Observable<any>{
    return this.http.get<any>(this.url).pipe(
      filter(
        usuario => usuario['results'][0].gender != 'male'
      )
    );
  }

  obtenerUsuarioCiudad(): Observable<any>{
    return this.http.get<any>(this.url).pipe(
      filter(
        usuario => usuario['results'][0].location['country'] != 'Canada'
      )
    );
  }

  obtenerCantidadElementos(): Observable<any>{
    return this.http.get<any>(this.url).pipe(
      take(1)
    );
  }

  obtenerFoto(): Observable<any>{
    return this.http.get<any>(this.url).pipe(
      map(resp => {
        //console.log(resp);
        return resp['results'].map((usuario: any)=>{
          return {
            name: usuario.name,
            picture: usuario.picture
          }
        })
      })
    )
  }

  obtenerDatos(): Observable<any> {
    return this.http.get<any>(this.url).pipe(
      map(resp => {
        return resp['results'].map((usuario:any)=>{
          return {
            name: usuario.name,
            picture: usuario.picture,
            email: usuario.email,
            login: usuario.login
          }
        })
      })
    )
  }
}


